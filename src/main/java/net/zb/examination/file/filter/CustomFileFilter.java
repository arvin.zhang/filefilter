package net.zb.examination.file.filter;

import cn.hutool.core.util.StrUtil;

import java.io.File;
import java.io.FileFilter;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/17, 初始化版本
 * @version 1.0
 **/
public class CustomFileFilter implements FileFilter {

	private String suffix;
	private Long fileSize;
	private Integer compare;


	public CustomFileFilter(String suffix, Long fileSize, Integer compare){
		this.suffix = suffix;
		this.fileSize = fileSize;
		this.compare = compare;
	}


	@Override
	public boolean accept(File pathname) {
		if(!pathname.isFile()){
			return false;
		}
		if(StrUtil.isNotEmpty(suffix) && !"i".equals(suffix)){
			if(!StrUtil.endWith(pathname.getName(), StrUtil.addPrefixIfNot(suffix, "."))){
				return false;
			}
		}
		if(compare != null){
			if(compare >= 0){
				return pathname.length()/1024 - fileSize >= 0;
			}
			return pathname.length()/1024 - fileSize < 0;
		}
		return true;
	}
}
