package net.zb.examination.file;

import net.zb.examination.file.filter.CustomFileFilter;

import java.io.File;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/17, 初始化版本
 * @version 1.0
 **/
public class FilterHelper {


	public static void filterFiles(String suffix, Long fileSize, Integer compare){
		File[] files = new File("D:\\").listFiles(new CustomFileFilter(suffix, fileSize, compare));
		for (File file : files){
			System.out.println("文件名:" + file.getName() + ";  文件大小:" + file.length()/1024.0 + "kb");
		}
	}

}
