package net.zb.examination.file;

import cn.hutool.core.util.StrUtil;

import java.util.Scanner;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/17, 初始化版本
 * @version 1.0
 **/
public class FilterMain {



	public static void main(String[] args){
		System.out.print("输入要过滤的文件后缀(i表示不过滤此条件):");
		Scanner scan = new Scanner(System.in);
		while(scan.hasNext()){
			String suffix = scan.next();
			System.out.print("输入要过滤的文件大小(i表示不过滤此条件):");
			String fileSize = scan.next();
			if(StrUtil.isEmpty(fileSize) || fileSize.equals("i")){
				FilterHelper.filterFiles(suffix, null, null);
			}else{
				FilterHelper.filterFiles(suffix, Long.parseLong(fileSize), 0);
			}
			System.out.print("输入要过滤的文件后缀(i表示不过滤此条件):");
		}
	}

}
